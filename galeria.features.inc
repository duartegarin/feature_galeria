<?php
/**
 * @file
 * galeria.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function galeria_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function galeria_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function galeria_node_info() {
  $items = array(
    'galeria' => array(
      'name' => t('Galeria'),
      'base' => 'node_content',
      'description' => t('Galeria para apresentação de imagens ou fotografias.'),
      'has_title' => '1',
      'title_label' => t('Nome da galeria'),
      'help' => '',
    ),
  );
  return $items;
}
