<?php
/**
 * @file
 * galeria.features.field.inc
 */

/**
 * Implementation of hook_field_default_fields().
 */
function galeria_field_default_fields() {
  $fields = array();

  // Exported field: 'node-galeria-field_galeria_imagem'
  $fields['node-galeria-field_galeria_imagem'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_galeria_imagem',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '1',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'galeria',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'galleryformatter',
          'settings' => array(
            'link_to_full' => 1,
            'link_to_full_style' => '0',
            'modal' => 'shadowbox',
            'slide_style' => 'galleryformatter_slide',
            'style' => 'Greenarrows',
            'thumb_style' => 'galleryformatter_thumb',
          ),
          'type' => 'galleryformatter_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_galeria_imagem',
      'label' => 'Imagem',
      'required' => 1,
      'settings' => array(
        'alt_field' => 1,
        'file_directory' => 'galeria',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '1 MB',
        'max_resolution' => '800x600',
        'min_resolution' => '640x480',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'medium',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '-4',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Imagem');

  return $fields;
}
