<?php
/**
 * @file
 * galeria.features.menu_custom.inc
 */

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function galeria_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-galeria
  $menus['menu-galeria'] = array(
    'menu_name' => 'menu-galeria',
    'title' => 'Galeria',
    'description' => 'Disponibiliza as funcionalidades da Galeria.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Disponibiliza as funcionalidades da Galeria.');
  t('Galeria');


  return $menus;
}
