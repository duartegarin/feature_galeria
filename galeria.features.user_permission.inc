<?php
/**
 * @file
 * galeria.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function galeria_user_default_permissions() {
  $permissions = array();

  // Exported permission: create galeria content
  $permissions['create galeria content'] = array(
    'name' => 'create galeria content',
    'roles' => array(
      0 => 'galeria',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any galeria content
  $permissions['edit any galeria content'] = array(
    'name' => 'edit any galeria content',
    'roles' => array(
      0 => 'galeria',
    ),
    'module' => 'node',
  );

  return $permissions;
}
