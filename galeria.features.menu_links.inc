<?php
/**
 * @file
 * galeria.features.menu_links.inc
 */

/**
 * Implementation of hook_menu_default_menu_links().
 */
function galeria_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-galeria:admin/galeria
  $menu_links['menu-galeria:admin/galeria'] = array(
    'menu_name' => 'menu-galeria',
    'link_path' => 'admin/galeria',
    'router_path' => 'admin/galeria',
    'link_title' => 'Editar Galerias',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-galeria:node/add/galeria
  $menu_links['menu-galeria:node/add/galeria'] = array(
    'menu_name' => 'menu-galeria',
    'link_path' => 'node/add/galeria',
    'router_path' => 'node/add/galeria',
    'link_title' => 'Criar nova galeria',
    'options' => array(
      'attributes' => array(
        'title' => 'Criar uma nova galeria',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Criar nova galeria');
  t('Editar Galerias');


  return $menu_links;
}
