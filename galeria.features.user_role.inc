<?php
/**
 * @file
 * galeria.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function galeria_user_default_roles() {
  $roles = array();

  // Exported role: galeria
  $roles['galeria'] = array(
    'name' => 'galeria',
    'weight' => '4',
  );

  return $roles;
}
